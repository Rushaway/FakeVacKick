# FakeVacKick

Kick a player from server with fake VAC notice

## Commands

- sm_fakevackick - Perform a fake vac kick on target

## Changelog :

- 2.0 Update to SM 1.11 and hide the Player left the game message.

## Credits :

Original author : Byte (https://forums.alliedmods.net/showthread.php?p=2317588)
